/*
Package ini reads simple, ini-style configuration files. Such files
consist of a number of named sections, each containing key/value
pairs where both key and value are strings.

Configuration files read by this package are treated as a sequence
of lines in UTF-8 encoding. Lines are separated by "\n" or "\r\n".
Each line is one of the following:

	- Blank: Lines containing only whitespace are ignored.
	- Comment: If the line's first non-whitespace character is ';',
	  the line is ignored.
	- Section start: If the line's first non-whitespace character
	  is '[', then its last non-whitespace character must be ']'.
	  Text between the two brackets is trimmed of surrounding
	  whitespace and used as the section name for subsequent
	  key/value pairs.
	- Key/value pair: All other lines must be key/value pairs. Such
	  lines must contain at least one '=' character. Text to the
	  left of the first '=' is used as the key name; text to the
	  right (which may include additional '=' characters) is used
	  as the value. Both key and value are trimmed of surrounding
	  whitespace.

Unicode letters in key and section names are mapped to lower case.
Any number of sections with the same name are treated as a single
section. If a key name appears more than once in the same section,
the last value will be used. Key/value pairs appearing before the
first section start have the empty string as their section name.

No other processing is performed. In particular, there is no quoting
or escaping mechanism, no line continuation mechanism, and no
variable expansion in values. Any Unicode character other than '\n'
may appear in values or section names. Any Unicode character other
than '\n' or '=' may appear in key names, but they may not start
with '['.
*/
package ini

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
)

// Config represents an ini-style configuration file. It is a map of named
// sections, each of which is a map from string names to string values.
// Values set before a section declaration are in section "".
type Config map[string]map[string]string

// LoadFile reads Config values from the named file.
func LoadFile(name string) (Config, error) {
	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return LoadReader(f)
}

// LoadReader reads Config values from r.
func LoadReader(r io.Reader) (Config, error) {
	c := make(Config)
	s := bufio.NewScanner(r)
	section := ""
	line := 0
	for s.Scan() {
		line++
		l := strings.TrimSpace(s.Text())
		if len(l) == 0 || l[0] == ';' {
			continue
		}
		if l[0] == '[' {
			if l[len(l)-1] != ']' {
				return nil, fmt.Errorf("ini: bad section header on line %d", line)
			}
			section = strings.TrimSpace(l[1 : len(l)-1])
			continue
		}
		parts := strings.SplitN(l, "=", 2)
		if len(parts) != 2 {
			return nil, fmt.Errorf("ini: missing '=' on line %d", line)
		}
		c.Set(section, strings.TrimSpace(parts[0]), strings.TrimSpace(parts[1]))
	}
	if err := s.Err(); err != nil {
		return nil, err
	}
	return c, nil
}

// SaveFile writes Config values to the named file.
func (c Config) SaveFile(name string) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = c.WriteTo(f)
	return err
}

// WriteTo writes Config values to w. Section and key names are sorted, so a
// given Config value will always be written as the same byte sequence.
func (c Config) WriteTo(w io.Writer) (n int64, err error) {
	var sections []string
	var keys []string
	var nn int
	out := func(v ...interface{}) bool {
		nn, err = fmt.Fprint(w, v...)
		n += int64(nn)
		return err == nil
	}
	// First, write values in the empty section.
	if _, ok := c[""]; ok {
		for k := range c[""] {
			keys = append(keys, k)
		}
		sort.StringSlice(keys).Sort()
		for _, k := range keys {
			if !out(k, "=", c[""][k], "\n") {
				return
			}
		}
		if !out("\n") {
			return
		}
	}
	for sn := range c {
		sections = append(sections, sn)
	}
	sort.StringSlice(sections).Sort()
	for _, sn := range sections {
		if sn == "" {
			continue
		}
		keys = keys[:0]
		for k := range c[sn] {
			keys = append(keys, k)
		}
		sort.StringSlice(keys).Sort()
		if !out("[", sn, "]\n") {
			return
		}
		for _, k := range keys {
			if !out(k, "=", c[sn][k], "\n") {
				return
			}
		}
		if !out("\n") {
			return
		}
	}
	return
}

// Set sets key in section to value. Both section and key are mapped to
// lower case.
func (c Config) Set(section, key, value string) {
	section = strings.ToLower(section)
	key = strings.ToLower(key)
	s, ok := c[section]
	if !ok {
		s = make(map[string]string)
		c[section] = s
	}
	s[key] = value
}

// Get gets the value of key in section. If there is no such key, ok will be false.
func (c Config) Get(section, key string) (value string, ok bool) {
	var s map[string]string
	s, ok = c[section]
	if !ok {
		return
	}
	value, ok = s[key]
	return
}

// Value returns the value of key in section, or fallback if there is no
// such key.
func (c Config) Value(section, key, fallback string) string {
	v, ok := c.Get(section, key)
	if !ok {
		return fallback
	}
	return v
}
