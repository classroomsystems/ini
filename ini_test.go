package ini

import (
	"bytes"
	"os"
	"reflect"
	"strings"
	"testing"
)

var simpleConfig = Config{
	"":    {"a": "b", "b": "c"},
	"foo": {"a": "c", "b": "d"},
	"bar": {"a": "d", "b": "e"},
}

var readWriteTests = []struct {
	text      string
	parsed    Config
	canonical bool
	err       string
}{
	{"", Config{}, true, ""},
	{" ; The next line has no newline\n a=b", Config{"": {"a": "b"}}, false, ""},
	{"a=b\n\n", Config{"": {"a": "b"}}, true, ""},
	{"[\x00a[silly\r]section]\r\n a\rsilly\x00key \t= \t\"\ta=silly=value \" \r\n", Config{"\x00a[silly\r]section": {"a\rsilly\x00key": "\"\ta=silly=value \""}}, false, ""},
	{"[\x00a[silly\r]section]\na\rsilly\x00key=\"\ta=silly=value \"\n", Config{"\x00a[silly\r]section": {"a\rsilly\x00key": "\"\ta=silly=value \""}}, false, ""},
	{
		`
			a=a
			A=b
			
			 [ BAR ]	
			a=d
			b=a
			
			[ Foo]
			a=c
			b=d
			
			[bar]
			B=e
			
			; The empty section name is the section with no name.
			[  	]
			B=c
		`,
		simpleConfig,
		false,
		"",
	},
	{
		"a=b\nb=c\n\n[bar]\na=d\nb=e\n\n[foo]\na=c\nb=d\n\n",
		simpleConfig,
		true,
		"",
	},
	{"noequals", Config{}, false, "ini: missing '=' on line 1"},
	{"[badsection", Config{}, false, "ini: bad section header on line 1"},
}

func TestReadWrite(t *testing.T) {
	var buf bytes.Buffer
	for i, test := range readWriteTests {
		parsed, err := LoadReader(strings.NewReader(test.text))
		if err != nil {
			if test.err == "" {
				t.Errorf("unexpected error from readWriteTests[%d]: %v", i, err)
			} else if err.Error() != test.err {
				t.Errorf("wrong error from readWriteTests[%d]: got %q expecting %q", i, err.Error(), test.err)
			}
			continue
		}
		if !reflect.DeepEqual(parsed, test.parsed) {
			t.Errorf("wrong value for readWriteTests[%d]: got %#v expecting %#v", i, parsed, test.parsed)
		}
		if test.canonical {
			(&buf).Reset()
			_, err = test.parsed.WriteTo(&buf)
			if err != nil {
				t.Errorf("unexpected error writing readWriteTests[%d]: %v", i, err)
				continue
			}
			out := (&buf).String()
			if out != test.text {
				t.Errorf("wrong output for readWriteTests[%d]: got %q expected %q", i, out, test.text)
			}
		}
	}
}

func TestFiles(t *testing.T) {
	os.Remove("testing.ini")
	err := simpleConfig.SaveFile("testing.ini")
	if err != nil {
		t.Errorf("unexpected error saving testing.ini: %v", err)
		return
	}
	conf, err := LoadFile("testing.ini")
	if err != nil {
		t.Errorf("unexpected error loading testing.ini: %v", err)
		return
	}
	if !reflect.DeepEqual(conf, simpleConfig) {
		t.Errorf("wrong value read from testing.ini: got %#v expecting %#v", conf, simpleConfig)
	}
	os.Remove("testing.ini")
	if err != nil {
		t.Errorf("unexpected error removing testing.ini: %v", err)
	}
}

var valueTests = []struct {
	section, key, fallback, value string
}{
	{"", "a", "", "b"},
	{"", "b", "", "c"},
	{"foo", "a", "", "c"},
	{"foo", "b", "", "d"},
	{"bar", "a", "", "d"},
	{"bar", "b", "", "e"},
	{"nosuch", "a", "salad", "salad"},
	{"foo", "nosuch", "salad", "salad"},
}

func TestValue(t *testing.T) {
	for _, test := range valueTests {
		value := simpleConfig.Value(test.section, test.key, test.fallback)
		if value != test.value {
			t.Errorf("bad value for [%s]%s: got %q expecting %q", test.section, test.key, value, test.value)
		}
	}
}
