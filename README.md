ini: A Go package for reading simple configuration files
========================================================

There are many Go packages to do this job, but this particular
bikeshed has the most pleasing color.
For new work, [TOML][] is almost certainly a better choice.

[TOML]: https://en.wikipedia.org/wiki/TOML

Installation
------------

	go install bitbucket.org/classroomsystems/ini

Usage
-----

See godoc for details.

[![GoDoc](https://godoc.org/bitbucket.org/classroomsystems/ini?status.png)](https://godoc.org/bitbucket.org/classroomsystems/ini)

Example
-------

	:::go
	package main
	
	import (
		"fmt"
		"os"
		
		"bitbucket.org/classroomsystems/ini"
	)
	
	func main() {
		config, err := ini.LoadFile("foo.ini")
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		fmt.Println(config.Value("section", "key", "default"))
	}

License
-------

Copyright (c) 2015 Classroom Systems, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
